plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.kotlinCocoapods)
    alias(libs.plugins.androidLibrary)
}

kotlin {
    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = "1.8"
            }
        }
    }
    iosX64()
    iosArm64()
    iosSimulatorArm64()

    cocoapods {
        summary = "Some description for the Shared Module"
        homepage = "Link to the Shared Module homepage"
        version = "1.0"
        ios.deploymentTarget = "16.0"
        framework {
            baseName = "shared"
            isStatic = true
        }
    }
    
    sourceSets {
        commonMain.dependencies {
            //put your multiplatform dependencies here
        }
        commonTest.dependencies {
            implementation(libs.kotlin.test)
        }
    }
}

tasks.register("prepareReleaseOfiOSXCFramework") {
    description = "Publish iOS framework to the Cocoa Repo"

    // Create Release Framework for Xcode
    dependsOn("packageDistribution")
}
tasks.create<Zip>("packageDistribution") {
    // Delete existing XCFramework
    delete("$rootDir/shared.xcframework")

    // Replace XCFramework File at root from Build Directory
    copy {
        from("$rootDir/shared/build/cocoapods/publish/release/shared.xcframework")
        into("$rootDir/shared.xcframework")
    }
}

android {
    namespace = "com.example.kmmlibrarycicdsample"
    compileSdk = 34
    defaultConfig {
        minSdk = 24
    }
}