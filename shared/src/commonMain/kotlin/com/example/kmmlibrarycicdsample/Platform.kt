package com.example.kmmlibrarycicdsample

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform