package com.example.kmmlibrarycicdsample

public class Greeting {
    private val platform: Platform = getPlatform()

    public fun greet(): String {
        return "Hello, ${platform.name}!"
    }

    public fun getLibraryName(): String {
        return "Hello, This is KMM Library Sample"
    }
}